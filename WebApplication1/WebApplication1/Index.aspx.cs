using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //To test for [conflict] pull before commit; To test for conflict
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //to test the changes on second attempt
            lblhidden.Text = txtName.Text;
        }
    }
}
